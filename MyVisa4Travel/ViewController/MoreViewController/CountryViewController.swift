//
//  CountryViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 18/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
@objc protocol CountryProtocol
{
    func CountryDetails(CountryName : String)
}
class CountryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
    var delegate : CountryProtocol!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Select Country"
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(CountryViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        tblObj.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        
        let header = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor = .white
        header.textLabel?.font = UIFont(name: "Roboto-Medium", size: 20)
        header.textLabel?.textColor = blue_color
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if section == 0
        {
            return "A"
        }
        else
        {
            return "B"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? CountryCell
        cell?.selectionStyle = .none
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                cell?.lblName.text = "Australia"
            }
            if indexPath.row == 1
            {
                cell?.lblName.text = "Austria"
            }
            if indexPath.row == 2
            {
                cell?.lblName.text = "Algeria"
            }
        }
        else
        {
            if indexPath.row == 0
            {
                cell?.lblName.text = "Bahamas"
            }
            if indexPath.row == 1
            {
                cell?.lblName.text = "Bahrain"
            }
            if indexPath.row == 2
            {
                cell?.lblName.text = "Bangladesh"
            }
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tblObj.cellForRow(at: indexPath) as! CountryCell
        delegate.CountryDetails(CountryName: cell.lblName.text!)
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class CountryCell: UITableViewCell
{
    @IBOutlet var lblName : UILabel!
}
