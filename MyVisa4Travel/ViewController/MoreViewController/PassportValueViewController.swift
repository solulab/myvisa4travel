//
//  PassportValueViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 17/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class PassportValueViewController: UIViewController,CountryProtocol {

    @IBOutlet var viewHeight : NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if !IS_IPHONE_4 {
            viewHeight.constant = UIScreen.main.bounds.size.height - 64
        }
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Passport Value"
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(PassportValueViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @IBAction func btnCountryPress(sender : UIButton)
    {
        let viewObj : CountryViewController = self.storyboard?.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        viewObj.delegate = self
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    func CountryDetails(CountryName : String)
    {
        
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
