//
//  MoreViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 08/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblObj.tableFooterView = UIView()

    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden=false
        self.title = "More"
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? MoreCell
        cell?.selectionStyle = .none
      
        if indexPath.row == 0
        {
            cell?.lblName.text = "Worldwide Embasies"
            cell?.imgView.image=#imageLiteral(resourceName: "worldwide_gray")
        }
        if indexPath.row == 1
        {
            cell?.lblName.text = "Passport Value"
            cell?.imgView.image=#imageLiteral(resourceName: "passport_gray")
        }
        if indexPath.row == 2
        {
            cell?.lblName.text = "Country Information"
            cell?.imgView.image=#imageLiteral(resourceName: "country_gray")
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let viewObj : WorldWideEmbasiesViewController = self.storyboard?.instantiateViewController(withIdentifier: "WorldWideEmbasiesViewController") as! WorldWideEmbasiesViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
        if indexPath.row == 1
        {
            let viewObj : PassportValueViewController = self.storyboard?.instantiateViewController(withIdentifier: "PassportValueViewController") as! PassportValueViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
        if indexPath.row == 2
        {
            let viewObj : CountryInformationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CountryInformationViewController") as! CountryInformationViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class MoreCell: UITableViewCell
{
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
}
