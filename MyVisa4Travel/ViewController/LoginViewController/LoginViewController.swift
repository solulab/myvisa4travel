//
//  LoginViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 17/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
@objc protocol LoginProtocol
{
    func LoginDetails(isLogin : Bool)
}
class LoginViewController: UIViewController
{

    @IBOutlet var viewHeight : NSLayoutConstraint!
    var delegate : LoginProtocol!
    @IBOutlet var scrView : UIScrollView!
    @IBOutlet var txtEmail : customBottomLineTextField!
    @IBOutlet var txtPassword : customBottomLineTextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Log in"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "cancel"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(LoginViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        txtEmail.text = "urja@solulab.com"
        txtPassword.text = "123456"

    }
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        if !IS_IPHONE_4
        {
            viewHeight.constant = UIScreen.main.bounds.size.height - 114
        }
       
        
    }
    func validationMethod() ->  Bool
    {
        if(txtEmail.text?.characters.count != 0)
        {
            if(UtilityClass.emailvalidate(txtEmail.text!) == true)
            {
                if(txtPassword.text?.characters.count != 0)
                {
                    return true
                }
                else
                {
                    UtilityClass.showAlert("Enter Password")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert("Enter Valid Email Address")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert("Enter Email Address")
            return false
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden=false
    }
    @IBAction func btnForgorPassword(sender : UIButton)
    {
        let viewObj : ForgotPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func btnRegisterPress(sender : UIButton)
    {
        let loginViewObj : RegisterViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(loginViewObj, animated: true)
    }
    @IBAction func btnLoginPress(sender : UIButton)
    {
        if validationMethod()
        {
            JsonClass.jsonCallBack("\(kLogin)", parameter: [k_emailId : txtEmail.text!, k_password : txtPassword.text! , k_deviceToken : "1234567895245" , k_deviceId : UIDevice.current.identifierForVendor!.uuidString , k_deviceType : UIDevice.current.model , k_lattitude : UserDefaults.standard.object(forKey: k_lattitude) as! String , k_longitude : UserDefaults.standard.object(forKey: k_longitude) as! String], view: self.view , sucessBlock: { (result:Any?) -> Void in
                //print(result)
                let dicInfo = result as? NSDictionary
                print(dicInfo!)
                _ = self.navigationController?.popViewController(animated: true)
                self.delegate.LoginDetails(isLogin: true)
            })
            {(error:Error?) -> Void in
                print(error!)
            }
        }
    }

    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
