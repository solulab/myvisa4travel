//
//  RegisterViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 17/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet var viewHeight : NSLayoutConstraint!
    @IBOutlet var txtFullName : customBottomLineTextField!
    @IBOutlet var txtPassword : customBottomLineTextField!
    @IBOutlet var txtConfirmpassword : customBottomLineTextField!
    @IBOutlet var txtMobile : customBottomLineTextField!
    @IBOutlet var txtEmail : customBottomLineTextField!
    @IBOutlet var btnAccept : UIButton!
    var isAccepted : Bool!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Register"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "cancel"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(RegisterViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
//        txtEmail.text = "urja@solulab.com"
//        txtPassword.text = "123456"
        
        isAccepted = false
        
    }
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        if !IS_IPHONE_4
        {
            viewHeight.constant = UIScreen.main.bounds.size.height - 114
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField == txtMobile)
        {
            if(UtilityClass.numericValue(forTextBox: string) == true)
            {
                let newLength = textField.text!.characters.count + string.characters.count - range.length
                return newLength <= 10
            }
            else
            {
                return false
            }
        }
        return true
    }
    @IBAction func btnLoginPress(sender : UIButton)
    {
//        let loginViewObj : LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        self.navigationController?.pushViewController(loginViewObj, animated: true)
        _ = self.navigationController?.popViewController(animated: true)
    }
    func validation() -> Bool {
        if (txtFullName.text?.characters.count != 0)
        {
            if(txtEmail.text?.characters.count != 0)
            {
                if(UtilityClass.emailvalidate(txtEmail.text!) == true)
                {
                    if(txtPassword.text?.characters.count != 0)
                    {
                        if(txtConfirmpassword.text?.characters.count != 0)
                        {
                            if(txtConfirmpassword.text == txtPassword.text)
                            {
                                if(txtMobile.text?.characters.count == 10)
                                {
                                    return true
                                }
                                else
                                {
                                    UtilityClass.showAlert("Please Enter Valid Mobile number")
                                    return false
                                }
                            }
                            else
                            {
                                UtilityClass.showAlert("Password Does not Match")
                                return false
                            }
                        }
                        else
                        {
                            UtilityClass.showAlert("Please Enter Confirm Password")
                            return false
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert("Please Enter Password")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert("Enter Valid Email Address")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert("Enter Email Address")
                return false
            }
            
        }
        else
        {
            UtilityClass.showAlert("Please Enter Full Name")
            return false
        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden=false
    }
    @IBAction func btnAcceptPress(sender : UIButton)
    {
        if isAccepted == true
        {
            isAccepted = false
            btnAccept.setImage(#imageLiteral(resourceName: "box"), for: .normal)
        }
        else
        {
            isAccepted = true
            btnAccept.setImage(#imageLiteral(resourceName: "boxactive"), for: .normal)
        }
    }
    
    @IBAction func btnRegisterPress(sender : UIButton)
    {
        print(txtFullName.text!)
        print(txtEmail.text!)
        print(txtMobile.text!)
        print(txtPassword.text!)

//        print(UserDefaults.standard.object(forKey: "deviceToken") as! String)
        print(UserDefaults.standard.object(forKey: "latitude") as! String)
        print(UserDefaults.standard.object(forKey: "longtitude") as! String)

        if validation()
        {
            print(kRegistration)
            JsonClass.jsonCallBack("\(kRegistration)", parameter: [k_fullName : txtFullName.text! , k_emailId : txtEmail.text! , k_mobile : txtMobile.text!, k_password : txtPassword.text! , k_deviceToken : "1234567895245" , k_deviceId : UIDevice.current.identifierForVendor!.uuidString , k_deviceType : UIDevice.current.model , k_lattitude : UserDefaults.standard.object(forKey: k_lattitude) as! String , k_longitude : UserDefaults.standard.object(forKey: k_longitude) as! String], view: self.view , sucessBlock: { (result:Any?) -> Void in
                //print(result)
                let dicInfo = result as? NSDictionary
                _ = self.navigationController?.popViewController(animated: true)
                
                UtilityClass.showAlert("\(dicInfo?.object(forKey: "message"))")
                print(dicInfo!)
            })
            {(error:Error?) -> Void in
                print(error!)
            }
            
        }
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
