//
//  ForgotPasswordViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 17/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController
{
    
    @IBOutlet var viewHeight : NSLayoutConstraint!
    var delegate : LoginProtocol!
    @IBOutlet var txtEmail : customBottomLineTextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        if !IS_IPHONE_4
        {
            viewHeight.constant = UIScreen.main.bounds.size.height - 108
        }
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden=true

    }
    @IBAction func btnResetPasswordPress(sender : UIButton)
    {
        if(txtEmail.text?.characters.count != 0)
        {
            if(UtilityClass.emailvalidate(txtEmail.text!) == true)
            {
                txtEmail.resignFirstResponder()
                _=self.navigationController?.popViewController(animated: true)
                UtilityClass.showAlert("New PIN is send to your Mobile Number or Email Address")
                
                //      ForgotPassword()
            }
            else
            {
                UtilityClass.showAlert("Enter Valid Email Address")
            }
        }
        else
        {
            UtilityClass.showAlert("Enter Email Address")
        }
    }

    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
