//
//  AllCategoryListTableViewCell.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 25/10/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class AllCategoryListTableViewCell: UITableViewCell,UICollectionViewDataSource
{
    @IBOutlet var collectionView : UICollectionView!
    
    var CellCount : Int!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        print("------- cell count \(CellCount!)")
        return CellCount
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
        
        let size : CGFloat
        if collectionView.tag == 6 || collectionView.tag == 8 || collectionView.tag == 10 || collectionView.tag == 12
        {
            cell.viewDistance.isHidden = true
            size = (UIScreen.main.bounds.size.width - 20)/3
            cell.imgWidth.constant = size
            cell.imgHight.constant = cell.imgWidth.constant - 10
        }
        else
        {
            cell.viewDistance.isHidden = false
            size = (UIScreen.main.bounds.size.width-50)/3
            cell.imgWidth.constant = size
            cell.imgHight.constant = cell.imgWidth.constant
        }
        cell.imgView.image = #imageLiteral(resourceName: "service")
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let size : CGFloat
        if collectionView.tag == 6 || collectionView.tag == 8 || collectionView.tag == 10 || collectionView.tag == 12
        {
            size = (UIScreen.main.bounds.size.width - 10)/3
            
            return CGSize.init(width: size, height: size + 50)
        }
        else
        {
            size = (UIScreen.main.bounds.size.width-50)/3
            return CGSize.init(width: size, height: size + 60)
        }

    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}

class CategoryCell: UICollectionViewCell
{
    @IBOutlet var imgHight: NSLayoutConstraint!
    @IBOutlet var imgWidth : NSLayoutConstraint!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var viewDistance : UIView!
}
