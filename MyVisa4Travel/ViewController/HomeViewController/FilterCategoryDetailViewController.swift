//
//  FilterCategoryDetailViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 03/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class FilterCategoryDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var tblcell1 : UITableViewCell!
    @IBOutlet var  tblcell2 : UITableViewCell!
    @IBOutlet var  tblcell3 : UITableViewCell!
    @IBOutlet var  tblcell4 : UITableViewCell!
    @IBOutlet var  tblcell5 : UITableViewCell!
    @IBOutlet var  tblcell6 : UITableViewCell!
    @IBOutlet var  tblcell7 : UITableViewCell!
    @IBOutlet var  tblcell8 : UITableViewCell!

    @IBOutlet var imgVisa : UIImageView!
    @IBOutlet var imgTexi : UIImageView!
    @IBOutlet var imgLawyers : UIImageView!
    @IBOutlet var imgColleges : UIImageView!
    @IBOutlet var imgAccomodation : UIImageView!
    @IBOutlet var imgTour : UIImageView!
    @IBOutlet var imgTravel : UIImageView!
    @IBOutlet var imgEnglish : UIImageView!
    @IBOutlet var imgPickup : UIImageView!
    
    @IBOutlet var lblVisa : UILabel!
    @IBOutlet var lblTaxi : UILabel!
    @IBOutlet var lblLawyers : UILabel!
    @IBOutlet var lblCollege : UILabel!
    @IBOutlet var lblAccomodation : UILabel!
    @IBOutlet var lblTour : UILabel!
    @IBOutlet var lblTravel : UILabel!
    @IBOutlet var lblEnglish : UILabel!
    @IBOutlet var lblPickup : UILabel!

    @IBOutlet var switchAvailable : UISwitch!
    @IBOutlet var switchRated : UISwitch!
    @IBOutlet var switchBookmark : UISwitch!

    var isVisaSelected : Bool!
    var isTaxiSelected : Bool!
    var isLawyerSelected : Bool!
    var isAccomodationSelected : Bool!
    var isCollegeSelected : Bool!
    var isTourSelected : Bool!
    var isTravelSelected : Bool!
    var isEnglishSelected : Bool!
    var isPickupSelected : Bool!

    @IBOutlet var tblHeight : NSLayoutConstraint!
    @IBOutlet var viewHeight : NSLayoutConstraint!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        super.viewWillAppear(true)
        
        if !IS_IPHONE_4
        {
            viewHeight.constant = UIScreen.main.bounds.size.height - 64
        }
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Filters"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(FilterCategoryDetailViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let button1: UIButton = UIButton.init(type:UIButtonType.custom)
        button1.setTitle("Reset", for: .normal)
        button1.addTarget(self, action: #selector(FilterCategoryDetailViewController.btnResetPress), for: UIControlEvents.touchUpInside)
        button1.titleLabel!.font =  UIFont(name: "Roboto-Regular", size: 17)
        button1.frame = CGRect.init(x: 0, y: 0, width: 50, height: 30)
        button1.imageEdgeInsets = UIEdgeInsetsMake(25, 25, 25, 25)
        
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItem = barButton1

        isVisaSelected = false
        isTourSelected = false
        isTaxiSelected = false
        isLawyerSelected = false
        isAccomodationSelected = false
        isCollegeSelected = false
        isEnglishSelected = false
        isTravelSelected = false
        isPickupSelected = false
        
        getData()
    }
    func getData() {
        tblObj.reloadData()
        
        tblHeight.constant = tblObj.contentSize.height
        viewHeight.constant =  tblHeight.constant - 30
        tblObj.isScrollEnabled = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        tableView.allowsSelection = false
        
        if indexPath.row == 0
        {
            return tblcell1
        }
        else if indexPath.row == 1
        {
            return tblcell2
        }
        else if indexPath.row == 2
        {
            return tblcell3
        }
        else if indexPath.row == 3
        {
            return tblcell4
        }
        else if indexPath.row == 4
        {
            return tblcell5
        }
        else if indexPath.row == 5
        {
            return tblcell6
        }
        else if indexPath.row == 6
        {
            return tblcell7
        }
        else
        {
            return tblcell8
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 50
        }
        else if indexPath.row == 1
        {
            return 50
        }
        else if indexPath.row == 2
        {
            return 50
        }
        else if indexPath.row == 3
        {
            return 50
        }
        else if indexPath.row == 4
        {
            return 50
        }
        else if indexPath.row == 5
        {
            return 85
        }
        else if indexPath.row == 6
        {
            return 85
        }
        else
        {
            return 85
        }
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnResetPress(sender:UIButton)
    {
        switchRated.isOn = false
        switchAvailable.isOn = false
        switchBookmark.isOn = false

        imgVisa.image = #imageLiteral(resourceName: "visa_gray")
        lblVisa.textColor = gray_color
        isVisaSelected = false
        
        imgTexi.image = #imageLiteral(resourceName: "tax")
        lblTaxi.textColor = gray_color
        isTaxiSelected = false
        
        imgLawyers.image = #imageLiteral(resourceName: "Lawyers_gray")
        lblLawyers.textColor = gray_color
        isLawyerSelected = false
        
        imgColleges.image = #imageLiteral(resourceName: "collages")
        lblCollege.textColor = gray_color
        isCollegeSelected = false
        
        imgAccomodation.image = #imageLiteral(resourceName: "accomodation_gray")
        lblAccomodation.textColor = gray_color
        isAccomodationSelected = false
        
        imgTour.image = #imageLiteral(resourceName: "tour_gray")
        lblTour.textColor = gray_color
        isTourSelected = false
        
        imgEnglish.image = #imageLiteral(resourceName: "english_gray")
        lblEnglish.textColor = gray_color
        isEnglishSelected = false
        
        imgTravel.image = #imageLiteral(resourceName: "travel_gray")
        lblTravel.textColor = gray_color
        isTravelSelected = false
        
        imgPickup.image = #imageLiteral(resourceName: "Money Transfer_gray")
        lblPickup.textColor = gray_color
        isPickupSelected = false

    }
    @IBAction func btnVisaPress(sender:UIButton)
    {
        if isVisaSelected == true
        {
            imgVisa.image = #imageLiteral(resourceName: "visa_gray")
            lblVisa.textColor = gray_color
            isVisaSelected = false
        }
        else
        {
            imgVisa.image = #imageLiteral(resourceName: "visaactive")
            lblVisa.textColor = blue_color
            isVisaSelected = true
        }
    }
    @IBAction func btnTaxiPress(sender:UIButton)
    {
        if isTaxiSelected == true
        {
            imgTexi.image = #imageLiteral(resourceName: "tax")
            lblTaxi.textColor = gray_color
            isTaxiSelected = false
        }
        else
        {
            imgTexi.image = #imageLiteral(resourceName: "taxiactive")
            lblTaxi.textColor = blue_color
            isTaxiSelected = true

        }
    }
    @IBAction func btnLawyerPress(sender:UIButton)
    {
        
        if isLawyerSelected == true
        {
            imgLawyers.image = #imageLiteral(resourceName: "Lawyers_gray")
            lblLawyers.textColor = gray_color
            isLawyerSelected = false
        }
        else
        {
            imgLawyers.image = #imageLiteral(resourceName: "Lawyersactive")
            lblLawyers.textColor = blue_color
            isLawyerSelected = true
        }
    }
    @IBAction func btnCollegesPress(sender:UIButton)
    {
        
        if isCollegeSelected == true
        {
            imgColleges.image = #imageLiteral(resourceName: "collages")
            lblCollege.textColor = gray_color
            isCollegeSelected = false
        }
        else
        {
            imgColleges.image = #imageLiteral(resourceName: "collagesactive")
            lblCollege.textColor = blue_color
            isCollegeSelected = true
        }
    }
    @IBAction func btnAccomodationPress(sender:UIButton)
    {
        
        if isAccomodationSelected == true
        {
            imgAccomodation.image = #imageLiteral(resourceName: "accomodation_gray")
            lblAccomodation.textColor = gray_color
            isAccomodationSelected = false
        }
        else
        {
            imgAccomodation.image = #imageLiteral(resourceName: "accomodationactive")
            lblAccomodation.textColor = blue_color
            isAccomodationSelected = true
        }

    }
    @IBAction func btnTourPress(sender:UIButton)
    {
        
        if isTourSelected == true
        {
            imgTour.image = #imageLiteral(resourceName: "tour_gray")
            lblTour.textColor = gray_color
            isTourSelected = false
        }
        else
        {
            imgTour.image = #imageLiteral(resourceName: "touractive")
            lblTour.textColor = blue_color
            isTourSelected = true
        }
    }
    @IBAction func btnEnglishPress(sender:UIButton)
    {
        if isEnglishSelected == true
        {
            imgEnglish.image = #imageLiteral(resourceName: "english_gray")
            lblEnglish.textColor = gray_color
            isEnglishSelected = false
        }
        else
        {
            imgEnglish.image = #imageLiteral(resourceName: "englishactive")
            lblEnglish.textColor = blue_color
            isEnglishSelected = true
        }
    }
    @IBAction func btnTravelPress(sender:UIButton)
    {
        if isTravelSelected == true
        {
            imgTravel.image = #imageLiteral(resourceName: "travel_gray")
            lblTravel.textColor = gray_color
            isTravelSelected = false
        }
        else
        {
            imgTravel.image = #imageLiteral(resourceName: "travelactive")
            lblTravel.textColor = blue_color
            isTravelSelected = true
        }
    }
    @IBAction func btnPickupPress(sender:UIButton)
    {
        if isPickupSelected == true
        {
            imgPickup.image = #imageLiteral(resourceName: "Money Transfer_gray")
            lblPickup.textColor = gray_color
            isPickupSelected = false
        }
        else
        {
            imgPickup.image = #imageLiteral(resourceName: "Money Transferactive")
            lblPickup.textColor = blue_color
            isPickupSelected = true
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
