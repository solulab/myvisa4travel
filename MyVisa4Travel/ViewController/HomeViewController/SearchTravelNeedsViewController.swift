//
//  SearchTravelNeedsViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 25/10/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class SearchTravelNeedsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
{
    @IBOutlet var tblcell1 : UITableViewCell!
    @IBOutlet var tblcell2 : UITableViewCell!
    @IBOutlet var tblcell3 : UITableViewCell!
    @IBOutlet var tblcell4 : UITableViewCell!
    @IBOutlet var txtsearch : customTextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden=true
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row == 0
        {
            return tblcell1
        }
        else if indexPath.row == 1
        {
            return tblcell2
        }
        else if indexPath.row == 2
        {
            return tblcell3
        }
        else
        {
            return tblcell4
        }
    }
    @IBAction func btnBackPress(sender : UIButton)
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
