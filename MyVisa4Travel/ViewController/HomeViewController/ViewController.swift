//
//  ViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 21/10/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
import CoreLocation
class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,CLLocationManagerDelegate
{
    @IBOutlet var tblcell1 : UITableViewCell!
    @IBOutlet var tblcell2 : UITableViewCell!
    @IBOutlet var tblcell3 : UITableViewCell!
    @IBOutlet var tblcell4 : UITableViewCell!
    @IBOutlet var tblcell5 : UITableViewCell!
    @IBOutlet var tblcell6 : UITableViewCell!
    @IBOutlet var tblcell7 : UITableViewCell!
    @IBOutlet var txtSearch : customTextField!
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var tblHeight : NSLayoutConstraint!
    @IBOutlet var scrBanner : UIScrollView!
    @IBOutlet var scrView : UIScrollView!
    @IBOutlet var pageControll : UIPageControl!
    @IBOutlet var viewHeight : NSLayoutConstraint!
    @IBOutlet var btnNext : UIButton!
    @IBOutlet var btnPrevious : UIButton!
    var locationManager: CLLocationManager!
    @IBOutlet var ViewHeader: UIView!

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation])
    {
        let latestLocation: CLLocation = locations[locations.count - 1]
        
        let lat = String(format: "%.4f",latestLocation.coordinate.latitude)
        print(lat)
        let long = String(format: "%.4f",latestLocation.coordinate.longitude)
        print(long)
        UserDefaults.standard.set(lat, forKey: k_lattitude)
        UserDefaults.standard.set(long, forKey: k_longitude)
        UserDefaults.standard.synchronize()
        locationManager.stopUpdatingLocation()
//        horizontalAccuracy.text = String(format: "%.4f",
//                                         latestLocation.horizontalAccuracy)
//        altitude.text = String(format: "%.4f",
//                               latestLocation.altitude)
//        verticalAccuracy.text = String(format: "%.4f",
//                                       latestLocation.verticalAccuracy)
//        
//        if startLocation == nil {
//            startLocation = latestLocation
//        }
//        
//        let distanceBetween: CLLocationDistance =
//            latestLocation.distance(from: startLocation)
//        
//        distance.text = String(format: "%.2f", distanceBetween)
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if !IS_IPHONE_4 {
            viewHeight.constant = UIScreen.main.bounds.size.height - 64
        }
        self.navigationController?.isNavigationBarHidden=true
        scrBanner.contentSize = CGSize.init(width: self.view.frame.width * 5, height: 200)
        
        var cnt : CGFloat = 0
        for i in 0...4
        {
            cnt = CGFloat(i) * self.view.frame.width
            let imgView : UIImageView = UIImageView(frame: CGRect.init(x: cnt, y: 0, width: self.view.frame.width, height: 200))
            imgView.tag = i
            imgView.alpha = 0.7
            imgView.image = #imageLiteral(resourceName: "banner")
            scrBanner.addSubview(imgView)
            
            let lbl1 : UILabel = UILabel(frame: CGRect.init(x: cnt, y: 120, width: self.view.frame.width, height: 20))
            lbl1.textColor = UIColor.white
            lbl1.text = "   " + "Your Travel Guide"
            lbl1.font = UIFont(name: "Roboto-Regular", size: 17)
            lbl1.tag = i
            scrBanner.addSubview(lbl1)
            
            let lbl2 : UILabel = UILabel(frame: CGRect.init(x: cnt, y: 142, width: self.view.frame.width-125, height: 25))
            lbl2.text = "  " + "ABC Travel Agency"
            lbl2.textColor = UIColor.white
            lbl2.adjustsFontSizeToFitWidth = true
            lbl2.font = UIFont(name: "Roboto-Medium", size: 21)
            lbl2.tag = i
            scrBanner.addSubview(lbl2)
            
            let lbl3 : UILabel = UILabel(frame: CGRect.init(x: (cnt + lbl2.frame.width) + 5 , y: 142, width: 110, height: 25))
            lbl3.layer.cornerRadius = 10
            lbl3.textColor = UIColor.white
            lbl3.backgroundColor = UIColor(red: 234/255, green: 83/255, blue: 0, alpha: 1.0)
            lbl3.textAlignment = .center
            lbl3.text = "Recommended"
            lbl3.font = UIFont(name: "Roboto-Medium", size: 14)
            lbl3.tag = i
            scrBanner.addSubview(lbl3)
            
            let btnSelect : UIButton = UIButton(frame: CGRect.init(x: cnt, y: 0, width: self.view.frame.width, height: 200))
            btnSelect.backgroundColor = UIColor.clear
            btnSelect.addTarget(self, action:#selector(btnBannerClick(sender:)), for: .touchUpInside)
            btnSelect.setTitle("", for: .normal)
            scrBanner.addSubview(btnSelect)
        }
        getData()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden=true
        AppDel.window?.bringSubview(toFront: ViewHeader)
    }
    
    func getData() {
        tblObj.reloadData()
        
        tblHeight.constant = tblObj.contentSize.height
        viewHeight.constant =  tblHeight.constant + 290
        tblObj.isScrollEnabled = false
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @IBAction func changePage(sender : UIPageControl)
    {
        let x = CGFloat(pageControll.currentPage) * scrBanner.frame.size.width
        scrBanner.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
    }
    @IBAction func btnNext(sender : UIButton)
    {
        if pageControll.currentPage < 5
        {
            let x = CGFloat(pageControll.currentPage + 1) * scrBanner.frame.size.width
            scrBanner.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
            pageControll.currentPage = pageControll.currentPage + 1
        }
        else
        {
            btnNext.isEnabled = false
            btnPrevious.isEnabled = true
        }
    }
    
    @IBAction func btnPrevious(sender : UIButton)
    {
        if pageControll.currentPage > 0
        {
            let x = CGFloat(pageControll.currentPage - 1) * scrBanner.frame.size.width
            scrBanner.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
            pageControll.currentPage = pageControll.currentPage - 1
        }
        else
        {
            btnNext.isEnabled = true
            btnPrevious.isEnabled = false
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let pageNumber = round(scrBanner.contentOffset.x / scrBanner.frame.size.width)
        pageControll.currentPage = Int(pageNumber)
        if pageControll.currentPage == 0
        {
            btnNext.isEnabled = true
            btnPrevious.isEnabled = false
        }
        if pageControll.currentPage >= 5
        {
            btnNext.isEnabled = false
            btnPrevious.isEnabled = true
        }
    }
    
    @IBAction func btnSearchClick(sender : UIButton)
    {
        let searchViewObj : SearchTravelNeedsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchTravelNeedsViewController") as! SearchTravelNeedsViewController
        self.navigationController?.pushViewController(searchViewObj, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 20
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0 || section == 1 || section == 2  || section == 3 || section == 4 || section == 5 || section == 19        {
            return 0
        }
        else if section == 6 || section == 8 || section == 10 || section == 12
        {
            return 35
        }
        else
        {
            return 46
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let viewHeader = UIView(frame: CGRect.zero)
        let lblHeader = UILabel(frame: CGRect(x: 15, y: 5, width: 250, height: 30))
        //        lblHeader.text = "TEST TEXT"
        lblHeader.font = UIFont(name: "Roboto-Regular", size: 16)

        let imgView = UIImageView.init(frame: CGRect.init(x: self.view.frame.width - 40, y: 10, width: 20, height: 20))
        //        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryHeaderTableViewCell
        imgView.contentMode = .scaleAspectFit
        
        let imgView1 = UIImageView.init(frame: CGRect.init(x: 15, y: 37, width: self.view.frame.width - 30, height: 1))
        imgView1.backgroundColor = UIColor.clear
        imgView1.alpha = 0.5
        if section == 6 || section == 8 || section == 10 || section == 12
        {
            viewHeader.backgroundColor = bg_color
            lblHeader.textColor = gray_color
            lblHeader.text = "SPONSORED"
            imgView.isHidden = true
        }
        else if section == 7
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text = "Visa Consultancy";
            imgView.image = #imageLiteral(resourceName: "visa")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 9
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "Colleges"
            imgView.image = #imageLiteral(resourceName: "Collage")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 11
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "Travel Agencies"
            imgView.image = #imageLiteral(resourceName: "travel")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 13
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "Tour Organizor"
            imgView.image = #imageLiteral(resourceName: "tour")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 14
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "Lawyers"
            imgView.image = #imageLiteral(resourceName: "Lawyers")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 15
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "English Coaching Institutes"
            imgView.image = #imageLiteral(resourceName: "english")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 16
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "Accomodation"
            imgView.image = #imageLiteral(resourceName: "Accomodation")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 17
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "Cab / Taxi Services"
            imgView.image = #imageLiteral(resourceName: "taxi")
            imgView1.backgroundColor = UIColor.lightGray
        }
        else if section == 18
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.textColor = UIColor.black
            lblHeader.textColor = lightBlack_color
            lblHeader.text =  "Money Transfer Services"
            imgView.image = #imageLiteral(resourceName: "Money Transfer")
            imgView1.backgroundColor = UIColor.lightGray
        }

        else
        {
            viewHeader.backgroundColor = UIColor.white
            lblHeader.text =  ""
            imgView.isHidden = true
        }
        //        return headerCell
        viewHeader.addSubview(lblHeader)
        viewHeader.addSubview(imgView)
        viewHeader.addSubview(imgView1)
        
        return viewHeader
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? AllCategoryListTableViewCell

        if indexPath.section == 0
        {
            return tblcell1
        }
        else if indexPath.section == 1
        {
            return tblcell2
        }
        else if indexPath.section == 2
        {
            return tblcell3
        }
        else if indexPath.section == 3
        {
            return tblcell4
        }
        else if indexPath.section == 4
        {
            return tblcell5
        }
        else if indexPath.section == 5
        {
            return tblcell6
        }
        else if indexPath.section == 19
        {
            return tblcell7
        }
        else if indexPath.section == 6 || indexPath.section == 8 || indexPath.section == 10 || indexPath.section == 12
        {
            cell?.backgroundColor = bg_color
            cell?.contentView.backgroundColor = bg_color
            cell?.CellCount = 20
            
            cell?.collectionView.isScrollEnabled = true
            
            if let layout: UICollectionViewFlowLayout = cell?.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
        }
        else if indexPath.section == 7 || indexPath.section == 9 || indexPath.section == 11
        {
            cell?.CellCount = 9
            cell?.collectionView.isScrollEnabled = false
            if let layout: UICollectionViewFlowLayout = cell?.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
        }
        else if indexPath.section == 13 || indexPath.section == 14 || indexPath.section == 15 || indexPath.section == 16 || indexPath.section == 17 || indexPath.section == 18
        {
            cell?.CellCount = 6
            cell?.collectionView.isScrollEnabled = false
            if let layout: UICollectionViewFlowLayout = cell?.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
        }
        cell?.collectionView.tag = indexPath.section
        cell?.collectionView.reloadData()
        return cell!
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let size = (UIScreen.main.bounds.size.width-60)/3 + 50 as CGFloat
        if indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5
        {
            return 50
        }
        else if indexPath.section == 19
        {
            return 100
        }
        else if indexPath.section == 6 || indexPath.section == 8 || indexPath.section == 10 || indexPath.section == 12
        {
            return size + 15
        }
        else if indexPath.section == 7 || indexPath.section == 9 || indexPath.section == 11
        {
            return 3*size + 70
        }
        else if indexPath.section == 13 || indexPath.section == 14 || indexPath.section == 15 || indexPath.section == 16 || indexPath.section == 17 || indexPath.section == 18
        {
            return 2*size + 40
        }
        else
        {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if section == 0 || section == 1 || section == 2 || section == 3 || section == 4 || section == 5 || section == 6 || section == 8 || section == 10 || section == 12 || section == 19
        {
            return 0
        }
        else
        {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let viewFooter = UIView(frame: CGRect.zero)
        
        viewFooter.backgroundColor = bg_color
        
        let lblFooter = PaddingLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 43))
        lblFooter.font = UIFont(name: "Roboto-Regular", size: 16)
        lblFooter.textAlignment = .right
        lblFooter.text = "See More >      "
        lblFooter.backgroundColor = UIColor.white
        lblFooter.textColor = UIColor(red: 0, green: 125/255, blue: 197/255, alpha: 1.0)
        
        let btnFooter = UIButton(frame: CGRect(x: 15, y: 3, width: self.view.frame.width - 30, height: 40))
        btnFooter.addTarget(self, action: #selector(btnCategoryClick(sender:)), for: .touchUpInside)

        btnFooter.setTitle("", for: .normal)
        
        let imgView1 = UIImageView.init(frame: CGRect.init(x: 0, y: 3, width: self.view.frame.width, height: 1))
        imgView1.backgroundColor = UIColor.lightGray
        imgView1.alpha = 0.5
        
        viewFooter.addSubview(lblFooter)
        viewFooter.addSubview(btnFooter)
        
        viewFooter.addSubview(imgView1)
        
        return viewFooter
    }
    @IBAction func btnBannerClick(sender : UIButton)
    {
        let ViewObj : BusinessDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailViewController") as! BusinessDetailViewController
        ViewObj.isBookmark = false
        self.navigationController?.pushViewController(ViewObj, animated: true)
    }
    @IBAction func btnCategoryClick(sender : UIButton)
    {
        let ViewObj : CategoryDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailViewController") as! CategoryDetailViewController
        self.navigationController?.pushViewController(ViewObj, animated: true)
    }

    @IBAction func btnSearchLocationClick(sender : UIButton)
    {
        let ViewObj : LocationSearchViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationSearchViewController") as! LocationSearchViewController
        self.navigationController?.pushViewController(ViewObj, animated: true)
    }

    @IBAction func backToTop(sender : UIButton)
    {
        scrView.setContentOffset(CGPoint.zero, animated: true)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
@IBDesignable class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 15.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
