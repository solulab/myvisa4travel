//
//  CategoryDetailViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 03/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class CategoryDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate
{
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var tblcell1 : UITableViewCell!
    @IBOutlet var tblObj : UITableView!
    var isPullRefresh = false
    var pageLimit : Int = PAGELIMIT
    var arrCount : Int!
    var parPage : Int = 0

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden=true
        self.arrCount = 5
        self.tblObj.addPullToRefresh(actionHandler: { () -> Void in
//            self.arrCount = 10
            self.arrCount = 5
            self.pageLimit = PAGELIMIT
            self.isPullRefresh=true
             Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.hidepullToRefresh), userInfo: nil, repeats: false)
            self.getData()
        })
        self.tblObj.addInfiniteScrolling { () -> Void in
            self.arrCount = self.arrCount + 5

            if(self.pageLimit >= self.arrCount)
            {
                
            }
            if(self.arrCount >= PAGELIMIT)
            {

                self.pageLimit = self.pageLimit + PAGELIMIT
                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.hidepullToRefresh), userInfo: nil, repeats: false)

                self.getData()
            }
            else
            {
                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.hidepullToRefresh), userInfo: nil, repeats: false)

                self.getData()
            }
        }
    }
    
    func hidepullToRefresh()
    {
        tblObj.reloadData()
        self.tblObj.pullToRefreshView.stopAnimating()
        self.tblObj.infiniteScrollingView.stopAnimating()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden=true

    }
    func getData()
    {
//        tblObj.reloadData()
//        tblHeight.constant = tblObj.contentSize.height
//        viewHeight.constant =  tblHeight.constant + 190
//        tblObj.isScrollEnabled = false
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrCount
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 190
        }
        else
        {
            return 250
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? CategoryDetailTableViewCell
        
        if indexPath.row == 0
        {
            return tblcell1
        }
        else
        {
            cell?.backgroundColor = bg_color
            cell?.contentView.backgroundColor = bg_color
            cell?.CellCount = 5
            cell?.collectionView.tag = indexPath.section
            cell?.collectionView.reloadData()
        }
        
        return cell!
    }
    //----------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let viewObj : BusinessDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailViewController") as! BusinessDetailViewController
        viewObj.isBookmark = false

        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let size : CGFloat
        
        size = (UIScreen.main.bounds.size.width)/3
        
        return CGSize.init(width: size, height: 160)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
        cell.imgView.image = #imageLiteral(resourceName: "service")
        return cell
    }
   
    @IBAction func btnFilter(sender:UIButton)
    {
        let ViewObj : FilterCategoryDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterCategoryDetailViewController") as! FilterCategoryDetailViewController
        self.navigationController?.pushViewController(ViewObj, animated: true)
    }
    
    @IBAction func btnBack(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
