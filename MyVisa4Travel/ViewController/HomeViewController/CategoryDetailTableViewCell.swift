//
//  CategoryDetailTableViewCell.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 03/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class CategoryDetailTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var lblRate : UILabel!
    @IBOutlet var lblDistance : UILabel!
    @IBOutlet var lblReview : UILabel!
    @IBOutlet var lblBookmark : UILabel!
    @IBOutlet var lblHours : UILabel!
    @IBOutlet var lblServices : UILabel!

    var CellCount : Int!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        print("------- cell count \(CellCount!)")
        return CellCount
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
        cell.imgView.image = #imageLiteral(resourceName: "service")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let size : CGFloat
      
        size = (UIScreen.main.bounds.size.width-25)/4
        return CGSize.init(width: size, height: size)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
class CategoryDetailCell: UICollectionViewCell
{
    @IBOutlet var imgView : UIImageView!

}
