//
//  ReportErrorViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 14/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class ReportErrorViewController: UIViewController,UITextViewDelegate
{
    @IBOutlet var txtReview : UITextView!

    @IBOutlet var viewHeight : NSLayoutConstraint!
    @IBOutlet var imgPhoneNumber : UIImageView!
    @IBOutlet var imgAddress : UIImageView!
    @IBOutlet var imgOutlet : UIImageView!
    @IBOutlet var imgService : UIImageView!

    var isPhoneNumber : Bool!
    var isAddress : Bool!
    var isOutlet : Bool!
    var isServices : Bool!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        if !IS_IPHONE_4
        {
            viewHeight.constant = UIScreen.main.bounds.size.height - 64
        }
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Report Error"
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(ReportErrorViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        isOutlet = false
        isPhoneNumber = false
        isAddress = false
        isServices = false
    }
    
    @IBAction func btnPhoneNumberPress(sender:UIButton)
    {
        if isPhoneNumber == true
        {
            imgPhoneNumber.image = #imageLiteral(resourceName: "box")
            isPhoneNumber = false
        }
        else
        {
            imgPhoneNumber.image = #imageLiteral(resourceName: "boxactive")
            isPhoneNumber = true
        }
    }
    @IBAction func btnAddressPress(sender:UIButton)
    {
        if isAddress == true
        {
            imgAddress.image = #imageLiteral(resourceName: "box")
            isAddress = false
        }
        else
        {
            imgAddress.image = #imageLiteral(resourceName: "boxactive")
            isAddress = true
        }

    }
    @IBAction func btnOutletPress(sender:UIButton)
    {
        if isOutlet == true
        {
            imgOutlet.image = #imageLiteral(resourceName: "box")
            isOutlet = false
        }
        else
        {
            imgOutlet.image = #imageLiteral(resourceName: "boxactive")
            isOutlet = true
        }
    }
    @IBAction func btnServicePress(sender:UIButton)
    {
        if isServices == true
        {
            imgService.image = #imageLiteral(resourceName: "box")
            isServices = false
        }
        else
        {
            imgService.image = #imageLiteral(resourceName: "boxactive")
            isServices = true
        }
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: ------------ TextView Delegate method ------------
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if("\(textView.text!)" == "Tap hear to give us some more details (optional)")
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if("\(textView.text!)" == "")
        {
            textView.text = "Tap hear to give us some more details (optional)"
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 }
