//
//  BusinessDetailViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 04/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit


class BusinessDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,LoginProtocol
{
    var header : StretchHeader!
    @IBOutlet var tableView : UITableView!
    var navigationView = UIView()
    @IBOutlet var tblHeight : NSLayoutConstraint!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var collectionServices : UICollectionView!
    @IBOutlet var collectionSponsors : UICollectionView!

    @IBOutlet var tblCellAddress : UITableViewCell!
    @IBOutlet var tblCellServices : UITableViewCell!
    @IBOutlet var tblCellOperatingHours : UITableViewCell!
    @IBOutlet var tblCellReportError : UITableViewCell!
    @IBOutlet var tblCellReviews : UITableViewCell!
    @IBOutlet var tblCellSponsers : UITableViewCell!

    var isLogin : Bool!
    var isBookmark : Bool!
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    func getData()
    {
        tableView.reloadData()
        tblHeight.constant = tableView.contentSize.height
    }
    func LoginDetails(isLogin: Bool)
    {
        if isLogin == true
        {
            if isBookmark == true
            {
                header.btnBookmark.setImage(#imageLiteral(resourceName: "bookmarksactive"), for: .normal)
                isBookmark = false
            }
            else
            {
                header.btnBookmark.setImage(#imageLiteral(resourceName: "bookmark"), for: .normal)
                isBookmark = true
            }
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        isLogin = false
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        setupHeaderView()
        
        // NavigationHeader
        let navibarHeight : CGFloat = navigationController!.navigationBar.bounds.height
        let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
        navigationView = UIView()
        navigationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: navibarHeight + statusbarHeight)
        navigationView.backgroundColor = blue_color
        navigationView.alpha = 0.0
        
        lblTitle = UILabel()
        lblTitle.textAlignment = .center
        lblTitle.frame = CGRect.init(x: (self.view.frame.width - 200)/2, y: 24, width: 200, height: 40)
//        lblTitle.center = navigationView.center
        lblTitle.font = UIFont(name: "Roboto-Regular", size: 20)
        lblTitle.textColor = UIColor.white
        lblTitle.text = "ABC Travel Agency"
        navigationView.addSubview(lblTitle)
        
        view.addSubview(navigationView)
        
        // back button
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 24, width: 30, height: 40)
        button.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate), for: UIControlState())
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(BusinessDetailViewController.leftButtonAction), for: .touchUpInside)
        view.addSubview(button)
        
        getData()
    }
    func setupHeaderView() {
        
        let options = StretchHeaderOptions()
        options.position = .fullScreenTop
        
        header = StretchHeader()
        header.stretchHeaderSize(headerSize: CGSize(width: view.frame.size.width, height: 250),
                                 imageSize: CGSize(width: view.frame.size.width, height: 250),
                                 controller: self,
                                 options: options)
        
        header.backgroundColor = UIColor.black
        header.imageView.image = #imageLiteral(resourceName: "banner")
        header.imageView.alpha = 0.7
        header.lblName.frame = CGRect.init(x: 20, y: 115, width: self.view.frame.width - 40, height: 23)
        header.lblName.text = "ABC Travel Agency"
        
        header.lblAddress.frame = CGRect.init(x: 20, y: 140, width: self.view.frame.width - 40, height: 23)
        header.lblAddress.text = "Satelite Road, Ahmedabad"
        
        header.RateView.frame = CGRect.init(x: 20, y: 165, width: 70, height: 20)
        
        header.imgLine.frame = CGRect.init(x: 10, y: 190, width: self.view.frame.width - 20, height: 1)

        header.btnCall.frame = CGRect.init(x: 20, y: 200, width: 30, height: 30)
        
        header.btnBookmark.frame = CGRect.init(x: 60, y: 200, width: 30, height: 30)
        header.btnBookmark.addTarget(self, action: #selector(btnBookmarkCall), for: .touchUpInside)
        
        header.btnShare.frame = CGRect.init(x: 100, y: 200, width: 30, height: 30)
        
        if isBookmark == true
        {
            header.btnBookmark.setImage(#imageLiteral(resourceName: "bookmarksactive"), for: .normal)
            isBookmark = false
        }
        else
        {
            header.btnBookmark.setImage(#imageLiteral(resourceName: "bookmark"), for: .normal)
            isBookmark = true
        }
        tableView.tableHeaderView = header
    }
    func btnBookmarkCall()
    {
        if isLogin == true
        {
            if isBookmark == true
            {
                header.btnBookmark.setImage(#imageLiteral(resourceName: "bookmarksactive"), for: .normal)
                isBookmark = false
            }
            else
            {
                header.btnBookmark.setImage(#imageLiteral(resourceName: "bookmark"), for: .normal)
                isBookmark = true
            }
        }
        else
        {
            let viewObj : LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            viewObj.delegate = self
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
    }
    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        header.updateScrollViewOffset(scrollView)
        
        // NavigationHeader alpha update
        let offset : CGFloat = scrollView.contentOffset.y
        if (offset > 50)
        {
            let alpha : CGFloat = min(CGFloat(1), CGFloat(1) - (CGFloat(50) + (navigationView.frame.height) - offset) / (navigationView.frame.height))
            navigationView.alpha = CGFloat(alpha)
            
            if alpha > 0.8
            {
                
            }
        }
        else
        {
            navigationView.alpha = 0.0;
        }
    }
    func leftButtonAction()
    {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            return tblCellAddress
        }
        else if indexPath.row == 1
        {
            return tblCellServices
        }
        else if indexPath.row == 2
        {
            return tblCellOperatingHours
        }
        else if indexPath.row == 3
        {
            return tblCellReportError
        }
        else if indexPath.row == 4
        {
            return tblCellReviews
        }
        else
        {
            return tblCellSponsers
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 140
        }
        else if indexPath.row == 1
        {
            return 220
        }
        else if indexPath.row == 2
        {
            return 220
        }
        else if indexPath.row == 3
        {
            return 60
        }
        else if indexPath.row == 4
        {
            return 420
        }
        else
        {
            return 200
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionServices
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryDetailCell
            
            cell.imgView.image = #imageLiteral(resourceName: "service")
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
            
            cell.imgView.image = #imageLiteral(resourceName: "service")
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if collectionView == collectionServices
        {
            let size = (UIScreen.main.bounds.size.width - 40)/3
            
            return CGSize.init(width: size, height: size)
        }
        else
        {
//            let size  = (UIScreen.main.bounds.size.width + 30)/3
//
//            return CGSize.init(width: size, height: size + 30)
           let size = (UIScreen.main.bounds.size.width)/3
            
            return CGSize.init(width: size, height: 160)
        }
    }
    @IBAction func btnAllReview(sender : UIButton)
    {
        let viewObj : MyReviewViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyReviewViewController") as! MyReviewViewController
        viewObj.isMyReview = false
        viewObj.strTitle = "ABC Travel"
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func btnReportError(sender : UIButton)
    {
        let viewObj : ReportErrorViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReportErrorViewController") as! ReportErrorViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
