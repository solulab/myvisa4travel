//
//  HomeViewController.swift
//
//  Created by Hetal Govani on 19/09/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class TabViewController: LUNTabBarController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden=true
//        self.tabBar.barTintColor = .white
        self.tabBar.isTranslucent = true
//        self.tabBar.tintColor = UIColor(red: 228.0 / 255.0, green: 232.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
//        self.tabBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)

        if let items = self.tabBar.items
        {
            for item in items
            {
                if let image = item.image
                {
                    item.image = image.withRenderingMode( .alwaysOriginal )
                }
            }
        }
//        self.tabBarController?.tabBar.tintColor=UIColor(red: 234.0 / 255.0, green: 83.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
       
    }
    
    override func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        let animationController = super.tabBarController(tabBarController, animationControllerForTransitionFrom: fromVC, to: toVC)
//        self.tabBarItem.title = nil
        tabBarItem.title = ""
        
//        tabBarItem.imageInsets = UIEdgeInsetsMake(9, 0, -9, 0)

        return animationController;

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
