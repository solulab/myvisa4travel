//
//  BookmarkViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 07/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class BookmarkViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
       override func viewDidLoad()
    {
        super.viewDidLoad()
        tblObj.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Bookmarks"

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? bookmarkCell
        cell?.selectionStyle = .none
//        cell?.backgroundColor = bg_color
//        cell?.contentView.backgroundColor = bg_color
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewObj : BusinessDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailViewController") as! BusinessDetailViewController
        viewObj.isBookmark = true
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class bookmarkCell: UITableViewCell {
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblRate : UILabel!
}
