//
//  AddReviewViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 08/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class AddReviewViewController: UIViewController,FloatRatingViewDelegate,UITextViewDelegate {

    @IBOutlet var rateView : FloatRatingView!
    @IBOutlet var lblRate : UILabel!
    @IBOutlet var txtReview : UITextView!
     
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(AddReviewViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        rateView.delegate = self
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden=false
        self.title = "ABC Visa Consultancy"
    }
    
    // MARK: ------------ TextView Delegate method ------------
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if("\(textView.text!)" == "Please Write at least 140 Characters about your personal experience at this company services. If the services hosted you and is expecting this review, please mention so.")
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if("\(textView.text!)" == "")
        {
            textView.text = "Please Write at least 140 Characters about your personal experience at this company services. If the services hosted you and is expecting this review, please mention so."
        }
    }
    
    // MARK: ------------ Button Action method ------------
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float)
    {
        lblRate.text = "\(rating)"
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
