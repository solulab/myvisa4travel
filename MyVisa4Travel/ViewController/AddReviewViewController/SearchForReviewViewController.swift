//
//  SearchForReviewViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 08/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class SearchForReviewViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblObj : UITableView!
    @IBOutlet var lblCurrentSearch : UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblObj.tableFooterView = UIView()

    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Add a Review"
        let lineView = UIView(frame: CGRect.init(x: 0, y: lblCurrentSearch.frame.size.height - 1, width: self.view.frame.size.width, height: 1))
        lineView.backgroundColor=UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 0.8)
        lblCurrentSearch.addSubview(lineView)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? bookmarkCell
        cell?.selectionStyle = .none
        //        cell?.backgroundColor = bg_color
        //        cell?.contentView.backgroundColor = bg_color
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewObj : AddReviewViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddReviewViewController") as! AddReviewViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
