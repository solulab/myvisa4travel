//
//  EditReviewViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 16/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class EditReviewViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(EditReviewViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonEdit: UIButton = UIButton.init(type:UIButtonType.custom)
        buttonEdit.setImage(#imageLiteral(resourceName: "edit1"), for: UIControlState.normal)
        buttonEdit.addTarget(self, action: #selector(EditReviewViewController.btnEditPress), for: UIControlEvents.touchUpInside)
        buttonEdit.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
//        buttonEdit.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let buttondelete: UIButton = UIButton.init(type:UIButtonType.custom)
        buttondelete.setImage(#imageLiteral(resourceName: "delet"), for: UIControlState.normal)
        buttondelete.addTarget(self, action: #selector(EditReviewViewController.btnDeletePress), for: UIControlEvents.touchUpInside)
        buttondelete.frame = CGRect.init(x: 30, y: 0, width: 33, height: 30)
//        buttondelete.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)

        let rightView : UIView = UIView(frame: CGRect.init(x: 10, y: 0, width: 63, height: 30))
        rightView.backgroundColor = UIColor.clear
        rightView.addSubview(buttonEdit)
        rightView.addSubview(buttondelete)
        
        let barbuttonEdit = UIBarButtonItem(customView: rightView)
        self.navigationItem.rightBarButtonItem = barbuttonEdit
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEditPress(sender:UIButton)
    {
        let viewObj : AddReviewViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddReviewViewController") as! AddReviewViewController
        
//        viewObj.rateView.rating = 3.0
//        viewObj.txtReview.text = "Review for ABC Agency"
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func btnDeletePress(sender:UIButton)
    {
        let alertController = UIAlertController(title: "MyVisa4Travel", message: "Are you sure you want to delete this review?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            _ = self.navigationController?.popViewController(animated: true)

            UtilityClass.showAlert("Review deleted successfully")
        }
        let DestructiveAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Destructive")
        }
        alertController.addAction(okAction)
        
        alertController.addAction(DestructiveAction)
        self.present(alertController, animated: true, completion: nil)

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
