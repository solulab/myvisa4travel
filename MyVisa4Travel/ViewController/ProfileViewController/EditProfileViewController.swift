//
//  EditProfileViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 15/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
import MobileCoreServices

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    @IBOutlet var viewHeight : NSLayoutConstraint!
    var imgPickerObj = UIImagePickerController()
    @IBOutlet var imgProfile : UIImageView!
    var isProfileImageSelected : Bool!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        if !IS_IPHONE_4 {
            viewHeight.constant = UIScreen.main.bounds.size.height - 64
        }
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Edit Profile"
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(EditProfileViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

    }
    @IBAction func profileButtonPress(sender: UIButton)
    {
        self.view.endEditing(true)
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Select Photo", message: nil, preferredStyle: .actionSheet)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Photos", style: .default)
        { action -> Void in
            
            self.imgPickerObj.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imgPickerObj.mediaTypes = [kUTTypeImage as String]
            self.imgPickerObj.allowsEditing = true
            self.imgPickerObj.delegate = self
            
            self.present(self.imgPickerObj, animated: true,completion: nil)
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                // print("Button capture")
                
                self.imgPickerObj.sourceType =
                    UIImagePickerControllerSourceType.camera
                self.imgPickerObj.mediaTypes = [kUTTypeImage as String]
                self.imgPickerObj.allowsEditing = true
                self.imgPickerObj.delegate = self
                self.present(self.imgPickerObj, animated: true,completion: nil)
            }
            else
            {
                UtilityClass.showAlert("Camera Not Avaliable")
            }
        }
        actionSheetController.addAction(deleteActionButton)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {
        imgProfile.image = image
        isProfileImageSelected=true
        imgProfile.layer.cornerRadius=imgProfile.frame.size.width/2
        
        picker.dismiss(animated: true)
        { () -> Void in
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
        { () -> Void in
        }
    }

    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
