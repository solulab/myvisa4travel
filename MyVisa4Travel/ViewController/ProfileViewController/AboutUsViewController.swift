//
//  AboutUsViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 16/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController
{
    @IBOutlet var viewHeight : NSLayoutConstraint!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if !IS_IPHONE_4
        {
            viewHeight.constant = UIScreen.main.bounds.size.height - 108
        }
        self.navigationController?.isNavigationBarHidden=false
        self.title = "About Us"
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(AboutUsViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
//    // MARK: ------------ TextView Delegate method ------------
//    func textViewDidBeginEditing(_ textView: UITextView)
//    {
//        if("\(textView.text!)" == "Your question or queries")
//        {
//            textView.text = ""
//        }
//    }
//    
//    func textViewDidEndEditing(_ textView: UITextView)
//    {
//        if("\(textView.text!)" == "")
//        {
//            textView.text = "Your question or queries"
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
