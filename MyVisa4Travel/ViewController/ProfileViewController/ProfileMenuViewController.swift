//
//  ProfileMenuViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 09/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class ProfileMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{

    @IBOutlet var tblObj : UITableView!
    @IBOutlet var tblCell : UITableViewCell!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblObj.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden=false
        self.title = "My Account"
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 0
        }
        else
        {
            return 44
        }

    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Roboto-Regular", size: 16)
        header.textLabel?.textColor = lightBlack_color

        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if section == 0
        {
            return ""
        }
        else if section == 1
        {
            return "Profile"
        }
        else
        {
            return "Others"
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return 70
        }
        else
        {
            return 44
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 2
        }
        else
        {
            return 6
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? ProfileMenuCell
        cell?.selectionStyle = .none
        if indexPath.section == 0 {
            if indexPath.row == 0
            {
                return tblCell
            }
        }
        else if indexPath.section == 1
        {
            if indexPath.row == 0
            {
                cell?.lblName.text = "Edit Profie"
                cell?.imgView.image=#imageLiteral(resourceName: "edit")
            }
            if indexPath.row == 1
            {
                cell?.lblName.text = "My Reviews"
                cell?.imgView.image=#imageLiteral(resourceName: "review")
            }
        }
        else
        {
            if indexPath.row == 0
            {
                cell?.lblName.text = "Contact Us"
                cell?.imgView.image=#imageLiteral(resourceName: "contact")
            }
            if indexPath.row == 1
            {
                cell?.lblName.text = "Share with Friends"
                cell?.imgView.image=#imageLiteral(resourceName: "share_gray")
            }
            if indexPath.row == 2
            {
                cell?.lblName.text = "Rate Us"
                cell?.imgView.image=#imageLiteral(resourceName: "rate_gray")
            }
            if indexPath.row == 3
            {
                cell?.lblName.text = "About Us"
                cell?.imgView.image=#imageLiteral(resourceName: "about")
            }
            if indexPath.row == 4
            {
                cell?.lblName.text = "Feedback"
                cell?.imgView.image=#imageLiteral(resourceName: "feedback")
            }
            if indexPath.row == 5
            {
                cell?.lblName.text = "Log Out"
                cell?.imgView.image=#imageLiteral(resourceName: "logout")
            }
        }
        return cell!
    }
    @IBAction func btnEditProfile(sender : UIButton)
    {
        let viewObj : EditProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0 {
            if indexPath.row == 0
            {
            }
        }
        else if indexPath.section == 1
        {
            if indexPath.row == 0
            {
                let viewObj : EditProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
                self.navigationController?.pushViewController(viewObj, animated: true)
            }
            if indexPath.row == 1
            {
                let viewObj : MyReviewViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyReviewViewController") as! MyReviewViewController
                viewObj.isMyReview = true
                viewObj.strTitle = "My Review(5)"
                self.navigationController?.pushViewController(viewObj, animated: true)
            }
        }
        else
        {
            if indexPath.row == 0
            {
                let viewObj : ContactUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                self.navigationController?.pushViewController(viewObj, animated: true)
            }
            if indexPath.row == 1
            {
            
            }
            if indexPath.row == 2
            {
            
            }
            if indexPath.row == 3
            {
                let viewObj : AboutUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                self.navigationController?.pushViewController(viewObj, animated: true)
            }
            if indexPath.row == 4
            {
                let viewObj : FeedbackViewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                self.navigationController?.pushViewController(viewObj, animated: true)
            }
            if indexPath.row == 5
            {
                let alertController = UIAlertController(title: "MyVisa4Travel", message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    
//                    let ViewObj : ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    navigationObj = UINavigationController(rootViewController: ViewObj)
//                    AppDel.window?.rootViewController = navigationObj
                    
//                    let TabViewOBj = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
//                    
//                    navigationObj = UINavigationController(rootViewController: TabViewOBj)
//                    
//                    AppDel.window?.rootViewController = navigationObj
                    
                    self.tabBarController?.selectedIndex = 0
                }
                let DestructiveAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                    print("Destructive")
                }
                alertController.addAction(okAction)
                
                alertController.addAction(DestructiveAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class ProfileMenuCell: UITableViewCell
{
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
}
