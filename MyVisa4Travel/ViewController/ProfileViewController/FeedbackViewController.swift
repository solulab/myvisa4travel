//
//  FeedbackViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 14/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController,UITextViewDelegate
{
    @IBOutlet var txtView : UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden=false
        self.title = "Feedback"
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(FeedbackViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    // MARK: ------------ TextView Delegate method ------------
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if("\(textView.text!)" == "Share what you love about this app, or what we could do better.")
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if("\(textView.text!)" == "")
        {
            textView.text = "Share what you love about this app, or what we could do better."
        }
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
