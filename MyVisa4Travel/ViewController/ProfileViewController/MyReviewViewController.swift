//
//  MyReviewViewController.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 14/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class MyReviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
    var isMyReview : Bool!
    var strTitle : String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden=false
        self.title = strTitle
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(MyReviewViewController.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        if isMyReview == false
        {
            let button: UIButton = UIButton.init(type:UIButtonType.custom)
            button.setImage(#imageLiteral(resourceName: "plus"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(MyReviewViewController.btnAddReviewPress), for: UIControlEvents.touchUpInside)
            button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
            button.imageEdgeInsets = UIEdgeInsetsMake(25, 25, 25, 25)
            
            let barButton = UIBarButtonItem(customView: button)
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as? MyReviewCell
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if isMyReview == true
        {
            let viewObj : EditReviewViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditReviewViewController") as! EditReviewViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddReviewPress(sender:UIButton)
    {
        let viewObj : AddReviewViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddReviewViewController") as! AddReviewViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class MyReviewCell: UITableViewCell
{
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblDay : UILabel!
    @IBOutlet var lblRate : UILabel!
    @IBOutlet var txtReview : UITextView!
}
