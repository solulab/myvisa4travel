//
//  AppDelegate.swift
//  MyVisa4Travel
//
//  Created by Hetal Govani on 21/10/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import UserNotifications

let blue_color = UIColor(red: 0.0/255.0, green: 125.0/255.0, blue: 197.0/255.0, alpha: 1.0)
let gray_color = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
let bg_color = UIColor(red: 236.0/255.0, green: 241.0/255.0, blue: 244.0/255.0, alpha: 1.0)
let orange_color = UIColor(red: 234.0 / 255.0, green: 83.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
let lightBlack_color = UIColor(red: 51.0 / 255.0, green: 51.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
var navigationObj : UINavigationController!
let PAGELIMIT : NSInteger = 10

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate
{

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        if(UIApplication.instancesRespond(to: #selector(UIApplication.registerUserNotificationSettings(_:)))) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        UINavigationBar.appearance().barTintColor = blue_color
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        UITabBar.appearance().tintColor = orange_color
        UITabBar.appearance().backgroundColor = UIColor(red: 228.0 / 255.0, green: 232.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 20)!,NSForegroundColorAttributeName : UIColor.white]
        let TabViewOBj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabViewController") as! TabViewController

        navigationObj = UINavigationController(rootViewController: TabViewOBj)
        Fabric.with([Crashlytics.self])

//        if #available(iOS 10.0, *)
//        {
////            let center  = UNUserNotificationCenter.current()
////            center.delegate = self
////            // set the type as sound or badge
////            center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
//                // Enable or disable features based on authorization
//                application.registerForRemoteNotifications()
//
////            }
//        } else {
//            // Fallback on earlier versions
//        }
        

        return true
    }
//    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let characterSet = CharacterSet(charactersIn: "<>")
//        let deviceTokenString = deviceToken.description.trimmingCharacters(in: characterSet).replacingOccurrences(of: " ", with: "");
//        print(deviceTokenString)
//        UserDefaults.standard.set(deviceTokenString, forKey: "deviceToken")
//        UserDefaults.standard.synchronize()
//    }
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print(error.localizedDescription)
//        print("Registration failed!")
//    }
//    
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
//        print("Handle push from foreground")
//        // custom code to handle push while app is in the foreground
//        print("\(notification.request.content.userInfo)")
//    }
//    
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print("Handle push from background or closed")
//        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
//        print("\(response.notification.request.content.userInfo)")
//    }
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

