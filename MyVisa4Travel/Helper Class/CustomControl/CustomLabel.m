//
//  CustomLabel.m
//
//  Created by Ankit on 3/29/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self setDesign];
}

-(void)setDesign
{
    self.layer.borderWidth = self.border;
    self.layer.borderColor = self.color.CGColor;
}

@end
