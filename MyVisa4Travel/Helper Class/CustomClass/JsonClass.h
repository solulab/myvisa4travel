//
//  JsonClass.h
//  JsonClassIntegration
//
//  Created by Hetal Govani on 26/09/15.
//  Copyright (c) 2015 SoluLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

#import "UtilityClass.h"

@interface JsonClass : NSObject

+(void)JsonCallBack:(NSString *)url
             parameter:(NSDictionary *)params
            view:(UIView *)view
           sucessBlock:(void (^)(id responseObject))sucess
          failureBlock:(void (^)(NSError *error))fail;

+(void)uploadImg:(NSString *)url
       parameter:(NSDictionary *)params
            view:(UIView *)view
        fileName:(NSString *)fileName
       ImageData: (NSData *)imageData
     sucessBlock:(void (^)(id responseObject))sucess
    failureBlock:(void (^)(NSError *error))fail;

@end
