//
//  JsonClass.m
//  JsonClassIntegration
//
//  Created by Hetal Govani on 26/09/15.
//  Copyright (c) 2015 SoluLab. All rights reserved.
//

#import "JsonClass.h"
#import "MBProgressHUD.h"

@implementation JsonClass

+(void)JsonCallBack:(NSString *)url
          parameter:(NSDictionary *)params
               view:(UIView *)view
        sucessBlock:(void (^)(id responseObject))sucess
       failureBlock:(void (^)(NSError *error))fail
{
    NSLog(@"%@",params);
    
    [MBProgressHUD showHUDAddedTo:view animated:YES];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager.requestSerializer setTimeoutInterval:60];
    NSLog(@"%@", url);
    [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress)
    {
        
    }
     
    success:^(NSURLSessionDataTask * _Nonnull task, id  responseObject)
    {
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        NSLog(@"dict : %@",dict);
        if ([dict objectForKey:@"StatusCode"])
        {
            NSInteger status = [[dict objectForKey:@"StatusCode"]integerValue];
            
            if (status == 200)
            {
                sucess([dict objectForKey:@"Success"]);
                if ([[dict objectForKey:@"Success"]objectForKey:@"message"])
                {
                    [UtilityClass showAlert:[[dict objectForKey:@"Success"]objectForKey:@"message"]];
                }
            }
            else if (status==400)
            {
                [UtilityClass showAlert:[[dict objectForKey:@"Failure"]objectForKey:@"message"]];
            }
        }
        else
        {
            sucess(dict);
        }
        [MBProgressHUD hideHUDForView:view animated:YES];
    }
    failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        [UtilityClass showAlert:error.localizedDescription];
        [MBProgressHUD hideHUDForView:view animated:YES];
        fail(error);
    }];
}

+(void)uploadImg:(NSString *)url parameter:(NSDictionary *)params view:(UIView *)view fileName:(NSString *)fileName ImageData:(NSData *)imageData sucessBlock:(void (^)(id))sucess failureBlock:(void (^)(NSError *))fail
{
    [MBProgressHUD showHUDAddedTo:view animated:YES];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager.requestSerializer setTimeoutInterval:60];

    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    {
        [formData appendPartWithFileData: imageData name:fileName fileName:[NSString stringWithFormat:@"%@.png",fileName] mimeType:@"image/png"];
        // etc.
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject)
    {
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        NSLog(@"dict : %@",dict);
        NSString *status = [dict objectForKey:@"status"];
        
        if ([status isEqualToString:@"200"])
        {
            sucess(dict);
        }
        else if ([status isEqualToString:@"100"])
        {
            [UtilityClass showAlert:[dict objectForKey:@"message"]];
        }
        else if ([status isEqualToString:@"300"])
        {
            [UtilityClass showAlert:[dict objectForKey:@"message"]];
        }
        else if ([status isEqualToString:@"400"])
        {
            [UtilityClass showAlert:[dict objectForKey:@"message"]];
        }
        [MBProgressHUD hideHUDForView:view animated:YES];
    }
    failure:^(NSURLSessionDataTask *task, NSError *error)
     {
        [UtilityClass showAlert:error.localizedDescription];
         [MBProgressHUD hideHUDForView:view animated:YES];
        fail(error);
     }];
}

@end
