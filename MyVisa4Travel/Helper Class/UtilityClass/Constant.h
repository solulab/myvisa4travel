//
//  Constant.h
//
//  Created by Ankit on 3/31/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

#ifndef Constant_h
#define Constant_h
#define google_key "172790535351-revbornrr4lqiipsuc4d7h62ee6jjstv.apps.googleusercontent.com"

#pragma mark - _____________________Login/Registration Keys _____________________

#define k_password "password"
#define k_emailId "emailId"
#define k_deviceToken "deviceToken"
#define k_deviceId "deviceId"
#define k_deviceType "deviceType"
#define k_longitude "longitude"
#define k_lattitude "lattitude"
#define k_fullName "fullName"
#define k_mobile "mobile"
#define k_lattitude "lattitude"
#define k_lattitude "lattitude"
#define k_lattitude "lattitude"
#define k_lattitude "lattitude"

//
//#pragma mark - _____________________User Profile Details Keys _____________________
//
//#define k_userName "userName"
//#define k_address "address"
//#define k_phoneNumber "phoneNumber"
//#define k_gender "gender"
//#define k_DOB "DOB"
//#define k_ProfilePic "profilePicture"
//#define k_coverPhoto "coverPhoto"
//
//#pragma mark - _____________________Update Profile Details Keys _____________________
//
//#define k_dateOfBirth "dateOfBirth"
//
//#pragma mark - _____________________Add user madication Keys _____________________
//
//#define k_add_UserMedicationId "UserMedicationId"
//#define k_add_medicationId "medicationId"
//#define k_add_medicationName "medicationName"
//#define k_add_instructions "instructions"
//#define k_add_writtenBy "writtenBy"
//#define k_add_quantity "quantity"
//#define k_add_rxNumber "rxNumber"
//#define k_add_startDate "startDate"
//#define k_add_writtenDate "writtenDate"
//#define k_add_perDose "perDose"
//#define k_add_perDay "perDay"
//#define k_add_howToTake "howToTake"
//#define k_add_originalRefills "originalRefills"
//#define k_add_medImage "medImage"
//#define k_add_new_frequencyPerDay "new_frequencyPerDay"
//#define k_add_reminder "reminder"
//#define k_add_isUpdate "isUpdate"
//#define k_add_time "time"
//#define k_add_new_time "new_time"
//#define k_add_del_case "del_case"
//#define k_add_del_time "del_time"
//
//#pragma mark - _____________________User Registration Keys _____________________
//
//#define k_socialId "socialId"
//#define k_socialType "socialType"
//#define k_profileImage "profileImage"
//
//#pragma mark - _____________________User Registration Keys _____________________
//
//#define k_fullName "fullName"
//#define k_emailId "emailId"
//#define k_mobileNo "mobileNo"
//#define k_password "password"
//#define k_device_token "device_token"
//
//#pragma mark - _______________ Add Health Data Keys ________________
//#define k_add_Data_userType "userType"
//#define k_add_Data_height "height"
//#define k_add_Data_weight "weight"
//#define k_add_Data_bloodPressures "bloodPressures"
//#define k_add_Data_heartRate "heartRate"
//#define k_add_Data_bloodGlucoses "bloodGlucoses"
//#define k_add_Data_LDL "LDL"
//#define k_add_Data_A1C "A1C"
//#define k_add_Data_HDL "HDL"
//#define k_add_Data_cholestrol "cholestrol"
//#define k_add_Data_triglycerides "triglycerides"
//
//
//#pragma mark - _______________Display Health Data/Past Data Keys ________________
//
//#define k_Data_Gender "Gender"
//#define k_Data_DateOfBirth "DateOfBirth"
//#define k_Data_Age "Age"
//
//#define k_Data_Height "Height"
//#define k_Data_Height_Info_userType "Height_Info_userType"
//#define k_Data_Height_Info_updatedBy "Height_Info_updatedBy"
//#define k_Data_Height_Info_updatedOn "Height_Info_updatedOn"
//
//#define k_Data_Weight "Weight"
//#define k_Data_Weight_Info_userType "Weight_Info_userType"
//#define k_Data_Weight_Info_updatedBy "Weight_Info_updatedBy"
//#define k_Data_Weight_Info_updatedOn "Weight_Info_updatedOn"
//
//#define k_Data_Blood_Pressures "Blood_Pressures"
//#define k_Data_Blood_Pressures_Info_userType "Blood_Pressures_Info_userType"
//#define k_Data_Blood_Pressures_Info_updatedBy "Blood_Pressures_Info_updatedBy"
//#define k_Data_Blood_Pressures_Info_updatedOn "Blood_Pressures_Info_updatedOn"
//
//#define k_Data_Heart_Rate "Heart_Rate"
//#define k_Data_Heart_Rate_Info_userType "Heart_Rate_Info_userType"
//#define k_Data_Heart_Rate_Info_updatedBy "Heart_Rate_Info_updatedBy"
//#define k_Data_Heart_Rate_Info_updatedOn "Heart_Rate_Info_updatedOn"
//
//#define k_Data_Blood_Glucoses "Blood_Glucoses"
//#define k_Data_Blood_Glucoses_Info_userType "Blood_Glucoses_Info_userType"
//#define k_Data_Blood_Glucoses_Info_updatedBy "Blood_Glucoses_Info_updatedBy"
//#define k_Data_Blood_Glucoses_Info_updatedOn "Blood_Glucoses_Info_updatedOn"
//
//#define k_Data_LDL "LDL"
//#define k_Data_LDL_Info_userType "LDL_Info_userType"
//#define k_Data_LDL_Info_updatedBy "LDL_Info_updatedBy"
//#define k_Data_LDL_Info_updatedOn "LDL_Info_updatedOn"
//
//#define k_Data_A1C "A1C"
//#define k_Data_A1C_Info_userType "A1C_Info_userType"
//#define k_Data_A1C_Info_updatedBy "A1C_Info_updatedBy"
//#define k_Data_A1C_Info_updatedOn "A1C_Info_updatedOn"
//
//#define k_Data_Cholestrol "Cholestrol"
//#define k_Data_Cholestrol_Info_userType "Cholestrol_Info_userType"
//#define k_Data_Cholestrol_Info_updatedBy "Cholestrol_Info_updatedBy"
//#define k_Data_Cholestrol_Info_updatedOn "Cholestrol_Info_updatedOn"
//
//#define k_Data_HDL "HDL"
//#define k_Data_HDL_Info_userType "HDL_Info_userType"
//#define k_Data_HDL_Info_updatedBy "HDL_Info_updatedBy"
//#define k_Data_HDL_Info_updatedOn "HDL_Info_updatedOn"
//
//#define k_Data_Triglycerides "Triglycerides"
//#define k_Data_Triglycerides_Info_userType "Triglycerides_Info_userType"
//#define k_Data_Triglycerides_Info_updatedBy "Triglycerides_Info_updatedBy"
//#define k_Data_Triglycerides_Info_updatedOn "Triglycerides_Info_updatedOn"
//
//#define k_Data_LastLogged "LastLogged"
//#define k_Data_updatedOn "updatedOn"
//#define k_Data_updatedBy "updatedBy"
//
//#define k_Data_pastData "pastData"
//
//
//#pragma mark - _____________________Add Allery Keys _____________________
//
//#define k_cause "cause"
//#define k_reaction "reaction"
//#define k_Allerydate "date"
//#define k_reaction_type "reaction_type"
//#define k_treat "treat"
//
//#pragma mark - _____________________Update Allery Keys _____________________
//
//#define k_userAllergyId "UserAllergy_Id"
//#define k_updateCause "Cause"
//#define k_updateReaction "Reaction"
//#define k_updateAllerydate "date"
//#define k_updateReaction_type "Reaction_type"
//#define k_updateTreat "Treat"
//
//
//#pragma mark - _____________________Add Reaction Keys _____________________
//
//#define k_Reaction_Id "Reaction_Id"
//#define k_medication_name "medication_name"
//#define k_drug_reaction "reaction"
//#define k_drug_reaction_date "reaction_date"
//#define k_drug_reaction_type "reaction_type"
//#define k_drug_treat "treat"
//
//#pragma mark - _____________________Add Reaction Keys _____________________
//
//#define k_UserHistory_Id "UserHistory_Id"
//#define k_MedicalCondition_Id "MedicalCondition_Id"
//#define k_Conditions "Conditions"
//#define k_MedicalCondition_Date "MedicalCondition_Date"
//
//
//#pragma mark - _____________________ Medation ViewController Keys _____________________
//
//#define k_Qunatity "Quantity"
//#define k_ReportedBy "ReportedBy"
//#define k_MedicationName "MedicationName"
//#define k_RefillRemaining "RefillRemaining"
//#define k_Last_Refill "lastRefilledOn"
//#define k_WrittenDate @"WrittenDate"
//#define k_ImagePath "ImagePath"
//#define k_Instruction @"Instructions"
//#define k_RX_Number @"RX_Number"
//#define k_PerDose @"PerDose"
//#define k_HowToTake @"HowToTake"
//#define k_StartDate @"StartDate"
//#define k_Frequency @"Frequency"
//#define k_WrittenBy @"WrittenBy"
//#define k_OriginalRefills @"originalRefills"
//#define k_DoctorAddress @"DoctorAddress"
//#define k_DoctorMobile @"DoctorMobile"
//#define k_DoctorEmail @"DoctorEmail"
//#define k_DoctorImage @"DoctorImage"
//
//#pragma mark - _____________________Doctor Profile Details Keys _____________________
//
//#define k__doc_address "Address"
//#define k_doc_phoneNumber "phoneNumber"
//#define k_doc_Zip "Zip"
//#define k_doc_City "City"
//#define k_doc_Country "Country"
//#define k_doc_speciality "speciality"
//#define k_doc_profileImage "ProfileImage"
//#define k_doc_emailAddress "emailAddress"
//
//
//#pragma mark - _____________________Doctor Profile update detail Keys _____________________
//
//#define k_doc_Mobile "Mobile"
//#define k_doc_Gender "Gender"
//#define k_doc_avl_hr_end_time "avl_hr_end_time"
//#define k_doc_avl_hr_start_time "avl_hr_start_time"

#endif /* Constant_h */
