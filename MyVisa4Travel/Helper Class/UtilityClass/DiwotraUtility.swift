//
//  MedSyncUtility.swift
//  MedsyncRX
//
//  Created by Ankit on 3/22/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

import Foundation
import UIKit
let AppDel =             UIApplication.shared.delegate as! AppDelegate

let IS_IPHONE_5 =        UIScreen.main.bounds.size.height == 568 ? true : false as Bool
let IS_IPHONE_4 =        UIScreen.main.bounds.size.height == 480 ? true : false as Bool
let IS_IPHONE_6 =        UIScreen.main.bounds.size.height == 667 ? true : false as Bool
let IS_IPHONE_6_plus =   UIScreen.main.bounds.size.height == 736 ? true : false as Bool
let IS_iPad =            UIScreen.main.bounds.size.height == 1024 ? true : false as Bool


let device_id =          UIDevice.current.identifierForVendor!.uuidString

let IS_IOS8_AND_UP  = (UIDevice.current.systemVersion as NSString).floatValue >= 8.0


func setShedowInCusomView(view:UIView)
{
    view.layer.masksToBounds = false;
    view.layer.shadowOffset = CGSize.init(width: 0, height: 10);
    view.layer.shadowRadius = 5;
    view.layer.shadowOpacity = 0.2;
}

func setShedowInCusomViewCell(view:UIView,shadow:CGFloat,opacity : Float)
{
    view.layer.masksToBounds = false;
    view.layer.shadowOffset = CGSize.init(width: 0, height: 0);
    view.layer.shadowRadius = shadow;
    view.layer.shadowOpacity = opacity;
}

func showAnimate(myView:UIView)
{
    myView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    myView.alpha=0;
    UIView.animate(withDuration: 0.25, animations: { () -> Void in
        myView.alpha = 1
        myView.transform = CGAffineTransform(scaleX: 1, y: 1)
    })
}

func removeAnimate(myView:UIView)
{
    UIView.animate(withDuration: 0.25, animations: { () -> Void in
        myView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3);
        myView.alpha = 0.0;
        }) { (finished:Bool) -> Void in
    }
}

/*func logout()
{
    
    if #available(iOS 8.0, *) {
        let refreshAlert = UIAlertController(title: "Sign Out", message: "Are You Sure to Sign Out ? ", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            
            JsonClass.JsonCallBack(klogout, parameter:[k_UserID:NSUserDefaults.standardUserDefaults().valueForKey(k_UserID)!,k_device_token:NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken")!] ,sucessBlock:
                { (result:AnyObject!) -> Void in
                    
                    print(Array(NSUserDefaults.standardUserDefaults().dictionaryRepresentation().keys).count)
                    
                    for key in Array(NSUserDefaults.standardUserDefaults().dictionaryRepresentation().keys)
                    {
                        if key == "DeviceToken"
                        {
                            
                        }
                        else
                        {
                            NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
                        }
                    }
                    
                    print(Array(NSUserDefaults.standardUserDefaults().dictionaryRepresentation().keys).count)
                    AppDel.viewControllerObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DoctorOrPatientViewController") as! DoctorOrPatientViewController
                    AppDel.navigationObj = UINavigationController(rootViewController: AppDel.viewControllerObj)
                    AppDel.navigationObj.navigationBarHidden = true
                    AppDel.sideMenuviewObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SideMenuViewController") as! SideMenuViewController
                    AppDel.SWSideMenureval = SWRevealViewController(rearViewController: AppDel.sideMenuviewObj, frontViewController: AppDel.navigationObj)
                    //                SWSideMenureval.delegate = self
                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: kIs_User_Login)
                    UIView.transitionWithView(AppDel.window!, duration: 0.5, options: .TransitionFlipFromLeft, animations: { () -> Void in
                        AppDel.window?.rootViewController = AppDel.SWSideMenureval
                        }, completion: nil)
                })
            { (error:NSError!) -> Void in
                
                print(error)
                JTProgressHUD.hide()
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action: UIAlertAction!) in
            
            refreshAlert .dismissViewControllerAnimated(true, completion: nil)
        }))
        
        UIApplication.sharedApplication().keyWindow?.rootViewController!.presentViewController(refreshAlert, animated: true, completion: nil)
    } else {
        // Fallback on earlier versions
    }
}*/
func systemToUtcDatestr(datestr : String,formate : String) -> (String)
{
    let formatter = DateFormatter();
    formatter.dateFormat = formate;
    
    let date = formatter.date(from: datestr)
    formatter.dateFormat = formate;

    formatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!;
    let utcTimeZoneStr = formatter.string(from: date!);
    return utcTimeZoneStr
}

func utcToSystemDatestr(date : String,formate : String) -> (String)
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formate//this your string date format
    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!;
    let dt = String(format: "%@ UTC",date)

    let date1 = dateFormatter.date(from: dt)
    
    dateFormatter.timeZone=NSTimeZone.local
    dateFormatter.dateFormat = formate
    
    let systemTimeZoneStr = dateFormatter.string(from: date1!)
    return systemTimeZoneStr
}
func systemToUtcDate(date : NSDate,formate : String) -> (String)
{

    let formatter = DateFormatter();
    formatter.dateFormat = formate;
   
    formatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!;
    let utcTimeZoneStr = formatter.string(from: date as Date);
    return utcTimeZoneStr
}

func utcToSystemDate(date : NSDate,formate : String) -> (String)
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formate//this your string date format
    dateFormatter.timeZone = NSTimeZone.local
    
    let Formatter = DateFormatter()
    Formatter.dateFormat = formate
    
    let systemTimeZoneStr = Formatter.string(from: date as Date)
    
    return systemTimeZoneStr
}
extension UITableView
{
    func setContentInset(top:CGFloat,bottom:CGFloat)
    {
       self.contentInset = UIEdgeInsets(top: top, left: 0, bottom: bottom, right: 0)
    }
}

extension String
{
    func trim() -> String
    {
    //    var str = self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var str = self.trimmingCharacters(in: NSCharacterSet.whitespaces)
        
        str = str.trimmingCharacters(in: NSCharacterSet.newlines)
        return str
    }
}

import Foundation
import UIKit  // don't forget this
@IBDesignable
class CustomNonPasteTextField: UITextField {
 //   override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
     //   if action == #selector(NSObject.paste(_:))
        if action == #selector(UIResponderStandardEditActions.paste(_:))
        {
            
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    @IBInspectable var borderWidth: CGFloat = 0
    {
        didSet
        {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor?
    {
        didSet
        {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor?
    {
        didSet
        {
            self.setValue(placeHolderColor, forKeyPath:"_placeholderLabel.textColor")
        }
    }
    
    @IBInspectable var paddingLeft: CGFloat?
    {
        didSet
        {
            let paddingView = UIView(frame:CGRect.init(x: paddingLeft!, y: 0, width: 30, height: 40))
            leftView=paddingView;
            leftViewMode = UITextFieldViewMode.always
        }
    }
    @IBInspectable var paddingRight: CGFloat?
    {
        didSet
        {
            let paddingView = UIView(frame:CGRect.init(x: paddingRight!, y: 0, width: 30, height: 40))
            rightView=paddingView;
            rightViewMode = UITextFieldViewMode.always
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }

}
